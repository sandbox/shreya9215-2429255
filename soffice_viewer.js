(function($) {
  $(window).load(function() {
    jQuery('#ifrm1').contents().find("html").find('img').each(function(){
      var url = $(this).attr('src');
      var new_url = url.replace('data:image/*', 'data:image/jpg');      
      $(this).attr('src', new_url);
    });
     jQuery('#ifrm1').contents().find("html").mousedown(function(event) {
      return false;
    });
  });
})(jQuery);